/*1) Це інтерфейс, який дозволяє програмістам змінювати структуру, вміст та стиль веб-сторінки з використанням JavaScript*/
/*2)innerHTML повертає HTML-код вмісту елемента, тоді як innerText повертає текстовий вміст елемента, без HTML-коду*/
/*3)Щоб звернутися до елемента сторінки можна використовувати методи document.getElementById(), document.getElementsByClassName(),
document.querySelector(), document.getElementsByName(), document.getElementsByTagName() або document.querySelectorAll().
Кожен метод має свої переваги та обмеження, тому кращий спосіб залежить від конкретної задачі*/

const paragraph = document.getElementsByTagName("p");
for (let i = 0; i < paragraph.length; i++) {
    paragraph[i].style.backgroundColor = "#ff0000";
}

const optionsList = document.getElementById("optionsList");
console.log(optionsList);

const parentElement = optionsList.parentElement;
console.log(parentElement);

const childList = optionsList.childNodes;
for (let i = 0; i < childList.length; i++) {
    const childNode = childList[i];
    console.log(childNode.nodeName, childNode.nodeType);
}

const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = 'This is a paragraph';

const mainHeader = document.querySelector('.main-header');
const elements = mainHeader.children;
console.log(elements);
for (let i = 0; i < elements.length; i++) {
    elements[i].classList.add('nav-item');
}

const title = document.querySelectorAll(".section-title");
title.forEach((title) => {
    title.classList.remove('section-title');
});